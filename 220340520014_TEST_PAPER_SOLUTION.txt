Section I 

1. Create table DEPT with the following structure:-
DEPTNO int(2)
DNAME varchar(15)
LOC varchar(10)
Insert the following rows into the DEPT table:-
10 ACCOUNTING NEW YORK
20 RESEARCH DALLAS
30 SALES CHICAGO
40 OPERATIONS BOSTON

ANS=> 

create table DEPT 
(DEPTNO int(2),
DNAME varchar(15),
LOC varchar(10));

insert into dept values
(10, 'ACCOUNTING', 'NEW YORK'),
(20, 'RESEARCH' ,'DALLAS'),
(30 ,'SALES' ,'CHICAGO'),
(40, 'OPERATIONS', 'BOSTON');

output-
10	ACCOUNTING	NEW YORK
20	RESEARCH	DALLAS
30	SALES	        CHICAGO
40	OPERATIONS	BOSTON
========================================================================================================================
2. Create table EMP with the following structure:-
EMPNO int(4)
ENAME varchar(10)
JOB varchar(9)
HIREDATE date
SAL float(7,2)
COMM float(7,2)
DEPTNO int(2)

Insert the following rows into the EMP table:-

ANS=>

create table EMP 
(EMPNO int(4),
ENAME varchar(10),
JOB varchar(9),
HIREDATE date,
SAL float(7,2),
COMM float(7,2),
DEPTNO int(2));

insert into emp values
(7839, 'KING', 'MANAGER', '1991-11-17', 5000, NULL, 10),
(7698, 'BLAKE', 'CLERK', '1981-05-01' ,2850, NULL, 30),
(7782,'CLARK' ,'MANAGER' ,'1981-06-09' ,2450 ,NULL ,10),
(7566,'JONES', 'CLERK' ,'1981-04-02', 2975, NULL, 20),
(7654,'MARTIN' ,'SALESMAN' ,'1981-09-28' ,1250 ,1400 ,30),
(7499,'ALLEN' ,'SALESMAN', '1981-02-20', 1600 ,300, 30);

OUTPUT->

7839	KING	MANAGER	   1991-11-17	5000.00		10
7698	BLAKE	CLERK	   1981-05-01	2850.00		30
7782	CLARK	MANAGER	   1981-06-09	2450.00		10
7566	JONES	CLERK	   1981-04-02	2975.00		20
7654	MARTIN	SALESMAN   1981-09-28	1250.00	1400.00	30
7499	ALLEN	SALESMAN   1981-02-20	1600.00	300.00	30
===========================================================================================================================
3. Display all the employees where SAL between 2500 and 5000 (inclusive of both).
ANS=>
select * from emp where sal >= 2500 and sal <= 5000;

OUTPUT->

7839	KING	MANAGER	1991-11-17	5000.00		10
7698	BLAKE	CLERK	1981-05-01	2850.00		30
7566	JONES	CLERK	1981-04-02	2975.00		20
=====================================================================================================
4. Display all the ENAMEs in descending order of ENAME.
ANS=>
select ename from emp order by 1 desc; 

OUTPUT->

MARTIN
KING
JONES
CLARK
BLAKE
ALLEN
========================================================================================================
5. Display all the JOBs in lowercase.
ANS=>
select lower(job) from emp;

OUTPUT->

manager
clerk
manager
clerk
salesman
salesman
========================================================================================================
6. Display the ENAMEs and the lengths of the ENAMEs.
ANS=>
select ename,length(ename) "length enmae" from emp;

OUTPUT->

KING	4
BLAKE	5
CLARK	5
JONES	5
MARTIN	6
ALLEN	5
=========================================================================================================
7. Display the DEPTNO and the count of employees who belong to that DEPTNO .
ANS=>
select count(ename) ,deptno from emp ,dept where emp.deptno = dept.deptno ;
=========================================================================================================
8. Display the DNAMEs and the ENAMEs who belong to that DNAME.
ANS=>
select dname,ename from emp,dept where emp.deptno = dept.deptno ;

OUTPUT->

ACCOUNTING  KING
SALES	    BLAKE
ACCOUNTING  CLARK
RESEARCH    JONES
SALES	    MARTIN
SALES	    ALLEN
============================================================================================================
9. Display the position at which the string ‘AR’ occurs in the ename.
ANS=>
select ename ,instr(ename,'ar') from emp;

OUTPUT->

KING	0
BLAKE	0
CLARK	3
JONES	0
MARTIN	2
ALLEN	0
===========================================================================================================
10. Display the HRA for each employee given that HRA is 20% of SAL.
ANS=>
select * , sal*0.2 "HRA" from emp;

OUTPUT->

7839	KING	MANAGER	        1991-11-17	5000.00		10	1000.00
7698	BLAKE	CLERK	        1981-05-01	2850.00		30	570.00
7782	CLARK	MANAGER	        1981-06-09	2450.00		10	490.00
7566	JONES	CLERK	        1981-04-02	2975.00		20	595.00
7654	MARTIN	SALESMAN	1981-09-28	1250.00	1400.00	30	250.00
7499	ALLEN	SALESMAN	1981-02-20	1600.00	300.00	30	320.00
====================================================================================================================
Section II 

1. Write a stored procedure by the name of PROC1 that accepts two varchar strings as parameters. Your procedure should 
then determine if the first varchar string exists inside the varchar string. For example, if string1 = ‘DAC’ and 
string2 = ‘CDAC, then string1 exists inside string2. The stored procedure should insert the appropriate 
message into a suitable TEMPP output table. Calling program for the stored procedure need not be written.

ANS=>

create table tempp(String_1 varchar(50), String_2 varchar(50),String_1_contain_String_2 varchar(50));

PROCEDURE->

delimiter //
create procedure proc1(x varchar(50),y varchar(50))
begin
if instr(x,y) = 0 then
insert into tempp values(x,y,'NO..DOES NOT CONTAIN');
else 
insert into tempp values(x,y,'YES..');
end if;
end; //
delimiter ;

CALLING PROCEDURE->

call proc1('cdac','dac');
call proc1('cdac','juhu');

OUTPUT->

cdac	dac	YES..
cdac	juhu	NO..DOES NOT CONTAIN

======================================================================================================================
2. Create a stored function by the name of FUNC1 to take three parameters, the sides of a triangle. 
The function should return a Boolean value:- TRUE if the triangle is valid, FALSE otherwise. A triangle is 
valid if the length of each side is less than the sum of the lengths of the other two sides. Check if the 
dimensions entered can form a valid triangle. Calling program for the stored function need not be written.

ANS=>

delimiter //
create function proc2(a int,b int,c int)
returns boolean
deterministic
begin
if (a<b+c and b<a+c and c<a+b) then return true;
else return false;
end if;
end ; //
delimiter ;
========================================================================================================================